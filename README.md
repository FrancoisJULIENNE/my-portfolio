# My Portfolio

This deposit demonstrates my skills with this tools : [node.js], [Bower], [Gulp].

### Installation
To deploy vendor's dependencies `JavaScript`, `css` and `Bootstrap` theme customization (css, js, fonts)..

We assume that [noed.js] is already installed.

You need `Gulp` installed with `global` option activated:

```sh
$ npm install --global gulp
```

Here how to install development's dependencies:
```sh
$ npm install
```
`package.json` source file (npm):
```json
{
  "name": "Portfolio-Francois-JULIENNE",
  "version": "0.0.1",
  "description": "Portfolio Francois JULIENNE",
  "dependencies": {
    "bower": "^1.5.3"
  },
  "devDependencies": {
    "gulp": "^3.9.0",
    "gulp-sass": "^2.0.4",
    "gulp-concat": "^2.6.0",
    "gulp-minify-css": "^1.2.1",
    "gulp-uglify": "^1.4.1",
    "gulp-rename": "^1.2.2",
    "gulp-uncss": "^1.0.4",
    "gulp-util": "^3.0.6",
    "del": "^2.0.2",
    "run-sequence": "^1.1.4"
  }
}
```

Then install the vendor's dependencies ([Twitter Bootstrap], [jQuery], etc...) and deploy customization files (js, sass, fonts).
```sh
$ gulp install
```
Vendor's dependencies are installed using [Bower] launched from a [Gulp] script `gulpfile.js`, see source [gulpfile.js (github source)] or [gulpfile.js (bitbucket source)] :

`bower.json` source file :
```json
{
  "name": "Portfolio Francois JULIENNE",
  "private": "true",
  "dependencies": {
      "bootstrap": "=3.3.5",
      "jquery": "=1.11.3",
      "modernizr": "=3.0.0",
      "normalize.css": "^3.0.3",
      "jquery_lazyload": "=1.9.1",
      "fastclick": "=1.0.6",
      "animate.css": "=3.1.0"
  }
}
```

The default `gulp` command executes `SASS` to `CSS` compilation of `Bootstrap` theme of my portfolio and minify the `JavaScript` file `main.js`.

### Watch

Then we add 3 `watch` with `gulp` command :

  - `gulp watch-sass` for compilation of `Bootstrap` theme of my portfolio in `./assets/sass/`
  - `gulp watch-mainjs` for minify (uglify) the `JavaScript` file `main.js` in `./assets/js/`
  - `gulp watch` executes both, `gulp watch-sass` and `gulp watch-mainjs`

   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [Gulp]: <http://gulpjs.com>
   [Bower]: <http://bower.io/>
   [gulpfile.js (bitbucket source)]: https://bitbucket.org/FrancoisJULIENNE/my-portfolio/src/62e0e45a2b06359248d49fcbd118abbdfb5f06c1/gulpfile.js?at=master&fileviewer=file-view-default
   [gulpfile.js (github source)]: https://github.com/FrancoisJULIENNE/my-portfolio/blob/master/gulpfile.js