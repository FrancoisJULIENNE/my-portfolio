;
(function() {

var mainApp = {
    getTypeDevice :function() {
        // init media tape
        var deviceAgent = navigator.userAgent.toLowerCase();
        var isTouchDevice =
        deviceAgent.match(/(iphone|ipod|ipad)/) ||
        deviceAgent.match(/(android)/)  ||
        deviceAgent.match(/(iemobile)/) ||
        deviceAgent.match(/iphone/i) ||
        deviceAgent.match(/ipad/i) ||
        deviceAgent.match(/ipod/i) ||
        deviceAgent.match(/blackberry/i) ||
        deviceAgent.match(/bada/i);

        return isTouchDevice ? "touchend" : "click";
    },
    initLazyLoad : function() {
        $("img.lazy").lazyload();
    },
    initTooltip : function() {
        $('.logofooter').tooltip({ animation: true, placement: "top" });
    },
    initAnimLogoBrand : function() {
        var removeClassEndAnimate = function() {
            $(this).removeClass('animated pulse');
        }
        $('.logo-anim').bind('mouseover',function() {
            $(this).addClass('animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', removeClassEndAnimate);
        });
    },
    initMenu : function() {
        var lastId,
            topMenu = $("#nav-principal"),
            topMenuHeight = topMenu.outerHeight()+15,
            // All list items
            menuItems = topMenu.find("a"),
            heightContact = $("#back-contact").innerHeight(),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function(){
              var item = $($(this).attr("href"));
              if (item.length) { return item; }
            }),
            clickEventType = this.getTypeDevice();

        //if low resolution screen (<=768), hide menu
        menuItems.bind(clickEventType,function() {
            if (Modernizr.mq('only screen and (max-width: 768px)')) {
                    $(".navbar-collapse").collapse('hide');
            }
        })
        //add class active on menu
        $("li[id^='nav0'").bind(clickEventType,function() {
            $("li[id^='nav0'").removeClass("active");
            $(this).addClass("active");
        });
        // Bind to scroll
        $(window).scroll(function(){
           // Get container scroll position
           var fromTop = $(this).scrollTop()+topMenuHeight;
           // Get id of current scroll item
           var cur = scrollItems.map(function(){
             if ($(this).offset().top < fromTop)
               return this;
           });
           // Get the id of the current element
           cur = cur[cur.length-1];
           var id = cur && cur.length ? cur[0].id : "";
           if (lastId !== id) {
               lastId = id;
               // Set/remove active class
               menuItems
                 .parent().removeClass("active")
                 .end().filter("[href=#"+id+"]").parent().addClass("active");
           }
           //is scroll bottom then active contact
           if((window.innerHeight + (window.scrollY + heightContact)) >= document.body.offsetHeight) {
                id="contact";
                lastId = id;
                menuItems
                 .parent().removeClass("active")
                 .end().filter("[href=#"+id+"]").parent().addClass("active");
           }
        });
    },
    formSubmit : function() {
        //form contact
        $('#formcontact').bind('submit', function(e) {
            e.preventDefault();
            $("#btn-submit button").prop("disabled", true);
            $(".erreur").empty();
            formData = $(this).serialize();
            $.ajax({
                url: "ajax/send_mail.php",
                data: formData,
                type: "POST",
                dataType: "json"
            }).done(function(data) {
                $("#btn-submit").hide();
                $(".erreur").html(data);
            }).fail(function() {
                $("#btn-submit button").prop("disabled", false);
                $(".erreur").text("Problème technique. Réessayez ultérieurement. Merci");
            });
        });
    },
    initMainApp : function() {
        //init main page
        this.initLazyLoad();
        this.initTooltip();
        this.initAnimLogoBrand();
        this.formSubmit();
        this.initMenu();
    }
}
mainApp.initMainApp();
})();