//test TypeScript
class DetectDevice {
  deviceAgent: string;
  isTouchDevice: any;
  constructor() {
    this.deviceAgent = navigator.userAgent.toLowerCase();
    this.isTouchDevice =
      this.deviceAgent.match(/(iphone|ipod|ipad)/) ||
      this.deviceAgent.match(/(android)/)  ||
      this.deviceAgent.match(/(iemobile)/) ||
      this.deviceAgent.match(/iphone/i) ||
      this.deviceAgent.match(/ipad/i) ||
      this.deviceAgent.match(/ipod/i) ||
      this.deviceAgent.match(/blackberry/i) ||
      this.deviceAgent.match(/bada/i);
  }
  detectTouchDevice() {
    return this.isTouchDevice ? "touchend" : "click";
  }
}

var detectDevice = new DetectDevice();

var clickEventType = detectDevice.detectTouchDevice();
console.log(clickEventType);
