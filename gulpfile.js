var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var less = require('gulp-less');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var uncss = require('gulp-uncss');
var runSequence = require('run-sequence').use(gulp);
var del = require('del');
var ts = require('gulp-typescript');

var paths = {
  sass: './assets/sass/*.scss',
  mainjs: './assets/js/main.js',
  maints: './assets/ts/*.ts'
};

//List of task
//
//task by default (gulp)
gulp.task('default', ['sass','mainjs']);

//install dependencies with bower and build (optimizes and install the js and css files)
gulp.task('install', function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    })
    .on('end',function() {
      gulp.start('build');
    });
});
//run all task with synchronously mode...
gulp.task('build', function() {
  //run sequence Synch
  runSequence(
    'mainjs',
    'install-modernizr-js',
    'install-vendor-js',
    'bootstrap-less',
    'install-all-css',
    'sass',
    'copyfonts',
    'del-bs-css-tmp'
    );
});

//task return sync
gulp.task('install-modernizr-js', function() {
  //to be alone in the header..
  return gulp.src([
    './assets/js/vendor/modernizr-custom.js'
  ])
  .pipe(uglify())
  .pipe(rename('modernizr.min.js'))
  .pipe(gulp.dest('./js'))
});


gulp.task('bootstrap-less', function() {
  //select less bootstrap and compile to css
  return gulp.src([
    './assets/less/vendor/bootstrap-custom.less'
  ])
  .pipe(less({
      compress: true
  }))
  .on('error', function(err) {
      console.log('Error with LESS.js', err.message);
  })
  .pipe(minifyCss({
    keepSpecialComments: 0
  }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./css'));
});


gulp.task('install-vendor-js', function() {
  //all vendor lib
  return gulp.src([
    './vendor/jquery/dist/jquery.min.js',
    './vendor/bootstrap/js/tooltip.js',
    './vendor/bootstrap/js/collapse.js',
    './vendor/bootstrap/js/transition.js',
    './vendor/jquery_lazyload/jquery.lazyload.js'
  ])
  .pipe(concat('vendor.min.js'))
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./js'))
});

gulp.task('install-all-css', function() {
  return gulp.src([
    './assets/css-fontface/fontface-theme.css',
    './vendor/animate.css/animate.min.css',
    './vendor/typopro-web/web/TypoPRO-OpenSans/TypoPRO-OpenSans-Regular.css',
    './vendor/typopro-web/web/TypoPRO-OpenSans/TypoPRO-OpenSans-Bold.css',
    './css/bootstrap-custom.css'
  ])
  .pipe(concat('./vendor.min.css'))
  .pipe(minifyCss({
    keepSpecialComments: 0
  }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./css'));
});


// sass to css
gulp.task('sass', function() {
  gulp.src(paths.sass)
    .pipe(sass({
      indentedSyntax: true,
      errLogToConsole: true,
      sourceComments : 'normal'
    }).on('error', sass.logError))
    .pipe(uncss({
      html: ['index.html']
    }))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./css'));
});

//main js minify
gulp.task('mainjs', function() {
  return gulp.src(paths.mainjs)
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./js'));
});

//copy fonts bootstrap and theme
gulp.task('copyfonts', function() {
  gulp.src('./assets/fonts/*')
    .pipe(gulp.dest('./fonts'));

  gulp.src('./vendor/bootstrap/fonts/*')
    .pipe(gulp.dest('./fonts'));
});
//delete bootstrap css tmp
gulp.task('del-bs-css-tmp', function() {
  return  del('./css/bootstrap-custom.css');
});

//define watch
gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.mainjs, ['mainjs']);
  gulp.watch(paths.maints, ['ts-compile']);
});

gulp.task('watch-sass', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('watch-mainjs', function() {
  gulp.watch(paths.mainjs, ['mainjs']);
});

gulp.task('watch-ts', function() {
  gulp.watch(paths.maints, ['ts-compile']);
});

// test TypeScript
gulp.task('ts-compile', function () {
  return gulp.src(paths.maints)
        .pipe(
          ts({
            noImplicitAny: true,
            out: 'main-ts.js'
        })
        )
        .pipe(gulp.dest('./js'));
});

//utility
gulp.task('clean-dir', function() {
  return  del(['!.gitignore','./css/*','./fonts/*','./js/*','./vendor/*']);
});
